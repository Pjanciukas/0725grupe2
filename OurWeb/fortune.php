<?php

$name = $_POST['name'];
$lastName = $_POST['lastName'];
$birthDate = $_POST['birthDate'];
$birthDate->format(m-d);

function getDates($startTime, $endTime) {
	// puts all dates between start date and end date to the array
    $day = 86400;
    $format = 'Y-m-d';
    $startTime = strtotime($startTime);
    $endTime = strtotime($endTime);
    $numDays = round(($endTime - $startTime) / $day);

    $array = array();

    for ($i = 1; $i < $numDays; $i++) {
        $array[] = date($format, ($startTime + ($i * $day)));
    }

    return $array;
}
function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
}

function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
}

$aquarius = getDates('2016-01-19', '2016-02-19');
$pisces = getDates('2016-02-18', '2016-03-21');
$aries = getDates('2016-03-20', '2016-04-20');
$taurus = getDates('2016-04-19', '2016-05-21');
$gemini = getDates('2016-05-20', '2016-06-21');
$cancer = getDates('2016-06-20', '2016-07-23');
$leo = getDates('2016-07-22', '2016-08-23');
$virgo = getDates('2016-08-22', '2016-09-23');
$libra = getDates('2016-09-22', '2016-10-23');
$scorpio = getDates('2016-10-22', '2016-11-22');
$sagittarius = getDates('2016-11-21', '2016-12-22');
$capricorn = getDates('2016-12-21', '2017-01-20');

if(birthDate == endsWith($aquarius, birthDate)) {
	echo "You are aquarius";
} else if (birthDate == endsWith($pisces, birthDate)) {
	echo "You are pisces";
} else if (birthDate == endsWith($aries, birthDate)) {
	echo "You are aries";
} else if (birthDate == endsWith($taurus, birthDate)) {
	echo "You are taurus";
} else if (birthDate == endsWith($gemini, birthDate)) {
	echo "You are gemini";
} else if (birthDate == endsWith($cancer, birthDate)) {
	echo "You are cancer";
} else if (birthDate == endsWith($leo, birthDate)) {
	echo "You are leo";
} else if (birthDate == endsWith($virgo, birthDate)) {
	echo "You are virgo";
} else if (birthDate == endsWith($libra, birthDate)) {
	echo "You are libra";
} else if (birthDate == endsWith($scorpio, birthDate)) {
	echo "You are scorpio";
} else if (birthDate == endsWith($sagittarius, birthDate)) {
	echo "You are sagittarius";
} else {
	echo "You are capricorn";
}
?>